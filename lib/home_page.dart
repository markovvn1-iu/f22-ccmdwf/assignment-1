import 'dart:async';

import 'package:assignment1/network/api.dart';
import 'package:assignment1/network/models.dart';
import 'package:assignment1/network/repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'elements/chuck_norris_logo.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key, required this.title});

  final String title;
  final ChuckNorrisJokesRepository jokesRepository = ChuckNorrisJokesRepository(
      api: ChuckNorrisApi(Dio(BaseOptions(contentType: "application/json", receiveTimeout: 10000))));

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ChuckNorrisJoke? joke;
  late Completer<ChuckNorrisJoke> nextJoke;
  ChuckNorrisLogoController logoController = ChuckNorrisLogoController();

  @override
  void initState() {
    super.initState();
    _loadNextJoke();
  }

  void _loadNextJoke() {
    nextJoke = widget.jokesRepository.random();
    if (!nextJoke.isCompleted) {
      setState(() => joke = null);
    }

    nextJoke.future.then((value) => setState(() => joke = value));
  }

  void _openLink(String urlLink) async {
    final Uri uri = Uri.parse(urlLink);
    if (!await canLaunchUrl(uri)) return;
    await launchUrl(uri, mode: LaunchMode.externalApplication);
  }

  Widget jokeBuilder(BuildContext context) {
    if (joke == null) {
      return const Center(child: Padding(padding: EdgeInsets.all(40), child: CircularProgressIndicator()));
    }
    return Column(children: <Widget>[
      Container(
        margin: const EdgeInsets.all(20),
        decoration: const BoxDecoration(
            color: Color.fromRGBO(0, 0, 0, 249),
            border: Border(left: BorderSide(color: Color.fromRGBO(0, 0, 0, 210), width: 5))),
        child: Padding(
            padding: const EdgeInsets.all(10),
            child: RichText(
                text: TextSpan(style: Theme.of(context).textTheme.bodyMedium, children: <InlineSpan>[
              const WidgetSpan(
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.0),
                child: Icon(Icons.format_quote, color: Color.fromRGBO(0, 0, 0, 210)),
              )),
              TextSpan(text: joke!.value),
            ]))),
      ),
      Container(
          alignment: Alignment.centerRight,
          margin: const EdgeInsets.only(right: 20),
          child: ElevatedButton(
              onPressed: () => _openLink(joke!.url),
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              ),
              child: Row(mainAxisSize: MainAxisSize.min, children: const <Widget>[
                Text(
                  "Open in browser ",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                ),
                Icon(Icons.open_in_browser)
              ])))
    ]);
  }

  void _showAboutDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("About"),
            content: Row(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
              const Text("👦"),
              Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: const [
                Text("Vladimir Markov", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("Email: Markovvn1@gmail.com"),
                Text("GitLab: @markovvn1")
              ])
            ]),
            actions: [TextButton(onPressed: () => Navigator.pop(context), child: const Text('OK'))]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
          child: Column(children: <Widget>[
        Container(
            padding: const EdgeInsets.fromLTRB(60, 20, 60, 0),
            child: GestureDetector(
                onLongPressDown: (_) => logoController.wiggle(),
                onDoubleTap: () => _openLink("https://api.chucknorris.io/"),
                child: ChuckNorrisLogo(
                    controller: logoController,
                    duration: const Duration(milliseconds: 700),
                    image: const Image(image: AssetImage('chucknorris_logo.png'))))),
        jokeBuilder(context),
        const Spacer(),
        Container(
            alignment: Alignment.bottomCenter,
            margin: const EdgeInsets.only(bottom: 40),
            child: ElevatedButton(
                onPressed: () {
                  logoController.wiggle();
                  if (nextJoke.isCompleted) _loadNextJoke();
                },
                onLongPress: () => _showAboutDialog(context),
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                ),
                child: const Text(
                  "🤠 Load next joke! 🤠", // button text message
                  style: TextStyle(color: Colors.white, fontSize: 12),
                ))),
      ])),
    );
  }
}
