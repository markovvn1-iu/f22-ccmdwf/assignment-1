import 'package:assignment1/network/models.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

part "api.g.dart";

@RestApi(baseUrl: "https://api.chucknorris.io/jokes")
abstract class ChuckNorrisApi {
  factory ChuckNorrisApi(Dio dio, {String baseUrl}) = _ChuckNorrisApi;

  @GET("/random")
  Future<ChuckNorrisJoke> random();
}
