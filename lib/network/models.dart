import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

@JsonSerializable()
class ChuckNorrisJoke {
  @JsonKey(name: 'icon_url')
  final String iconUrl;
  final String id;
  final String url;
  final String value;

  ChuckNorrisJoke(this.iconUrl, this.id, this.url, this.value);

  factory ChuckNorrisJoke.fromJson(Map<String, dynamic> json) => _$ChuckNorrisJokeFromJson(json);

  Map<String, dynamic> toJson() => _$ChuckNorrisJokeToJson(this);
}
