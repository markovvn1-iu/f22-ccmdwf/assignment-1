import 'dart:async';
import 'dart:collection';
import 'dart:math';

import 'package:assignment1/network/api.dart';
import 'package:assignment1/network/models.dart';
import 'package:flutter/material.dart';

class ChuckNorrisJokesRepository {
  static const int maxPreloadedJokes = 25;

  ChuckNorrisJokesRepository({required this.api}) {
    for (int i = 0; i < maxPreloadedJokes; i++) {
      Completer<ChuckNorrisJoke> joke = Completer<ChuckNorrisJoke>();
      _loadedJokes.addLast(joke);
      _jokesToLoad.addLast(joke);
    }
  }

  final ChuckNorrisApi api;

  final Queue<Completer<ChuckNorrisJoke>> _loadedJokes = Queue<Completer<ChuckNorrisJoke>>();
  final Queue<Completer<ChuckNorrisJoke>> _jokesToLoad = Queue<Completer<ChuckNorrisJoke>>();
  bool _doesLoadingWorkerExist = false;
  int _nextLoadingTime = DateTime.now().millisecondsSinceEpoch;

  Completer<ChuckNorrisJoke> random() {
    Completer<ChuckNorrisJoke> newJoke = Completer<ChuckNorrisJoke>();
    _loadedJokes.addLast(newJoke);
    _jokesToLoad.addLast(newJoke);
    _runLoadingWorker();
    return _loadedJokes.removeFirst();
  }

  Future _sleepToPreventDOS(int milliseconds) async {
    int currentTime = DateTime.now().millisecondsSinceEpoch;
    int sleepFor = max(0, _nextLoadingTime - currentTime);
    debugPrint("Sleeping for $sleepFor milliseconds to prevent DOS");
    _nextLoadingTime = currentTime + sleepFor + milliseconds;
    return Future.delayed(Duration(milliseconds: sleepFor));
  }

  Future<ChuckNorrisJoke> _loadRandomJoke() async {
    debugPrint("Start loading new joke");

    while (true) {
      await _sleepToPreventDOS(1000);

      try {
        return await api.random();
      } catch (err) {
        debugPrint("$err");
        debugPrint("Failed to load joke. Try again");
      }
    }
  }

  Future _runLoadingWorker() async {
    if (_doesLoadingWorkerExist) return;
    _doesLoadingWorkerExist = true;

    while (_jokesToLoad.isNotEmpty) {
      ChuckNorrisJoke joke = await _loadRandomJoke();
      debugPrint("New joke is loaded: $joke");
      _jokesToLoad.removeFirst().complete(joke);
    }

    _doesLoadingWorkerExist = false;
  }
}
